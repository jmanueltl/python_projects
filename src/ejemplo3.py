'''
Created on 24 ene. 2019

@author: Juan Tipismana
'''

valor1 = int(input("Primera nota : "))
valor2 = int(input("Segunda nota : "))
valor3 = int(input("Tercera nota : "))

prom = (valor1 + valor2 + valor3)/3

if prom>=15:
    print("Aprobado, muy bien "+str(prom))
else:
    if prom>=10.5:
        print("Aprobado ajustando "+str(prom))
    else:
        print("reprobado "+ str(prom))    
    