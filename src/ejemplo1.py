'''
Created on 24 ene. 2019

@author: Juan Tipismana
'''
valor1 = int(input("Primera nota : "))
valor2 = int(input("Segunda nota : "))
valor3 = int(input("Tercera nota : "))

prom = ( valor1+valor2+valor3)/3

if(prom>=18):
    print("Nivel sobresaliente "+str(prom))
elif(prom>=14 and prom<18):
    print("Nivel bueno "+str(prom))
elif(prom>=11 and prom<14):
    print("Nivel normal "+str(prom))
elif(prom>8 and prom<11):
    print("Falta ajustar "+str(prom))
else: 
    print("Malo "+str(prom))
    